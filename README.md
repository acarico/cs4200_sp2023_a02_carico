# Austin Carico



## Bachelor of Science in Computer Science, specialized in Computational Data Science

<b> Define the following terms in your own words: agent, environment, sensor, actuator, percept, agent function, and the agent program. </b> <br> 
Agent: Something that makes an observation and/or takes in some type of input and formulates an understanding of the input, and then makes a decision off the information it receives. <br>
Environment: This is what surrounds agents and decision makers and provides the background to their understanding as to what to take into account when making decisions. <br>
Sensor: This is what an agent uses to take in information on a certain aspect of the environment its getting information from. <br>
Actuator: The background to what allows a sensor to be able to take in information from the environment, without it, the sensor would not be able to function properly to bring in information. <br>
Percept: The enviroment provides this specific piece of information to to an agent's sensors. <br>
Agent function: When the agent's sensor takes in a percept, this is what it is called when the agent takes in the history of the percepts it receives (percept sequence) and uses this sequence to perform an action. <br>
Agent Program: This takes the agent function and runs it within an actual physical system. <br> <br>

<b> For the following agents, specify the sensors, actuators, and environment: microwave oven, chess program, and autonomous supply delivery plane. (Undergraduate students pick two out of three and graduate students need to take all three).</b> <br>
Microwave oven: Sensors: magnetron device, antenna, keypad, display Actuators: power cord Environment: households, kitchens, residents/guests <br>
Autonomous Suply Delivery Plane:  Sensors: GPS, object detectors, video, keyboard Actuators: Speed controller, Propellers(in most planes), LED systems, onboard speakers Environment: Airport, dir traffic, delivery warehouses <br> <br>

<b> An agent that senses only partial information about the state cannot be perfectly rational. </b>\
False, A rational agent makes decisions based off the information it receives at its sensors, and can make logical assumptions about the environment to understand it, based off how sufficient the information is. For example, a financial investment agent knows about a given set of companies and when they make a profit, but it does not know which has a net loss in profits for the month. It can make an inference on these companies by finding who has broke even or went above in profits, and see which companies are not in that set. <br>

<b>There exists a task environment in which every agent is rational. </b> <br>
True, when an environment is never changing (i.e has only one state) then the agent can achieve its goal no matter the action it takes. For example an automated car moving in an unchanging circular track will always achieve its goal given the number of laps it is set to make.

<b> The input to an agent program is the same as the input to the agent function. </b>
False, In the function, the percept sequence is taken in as input but for the program, only current percept is used to perform the physical action. For example, a vacuum claner program takes in current information on what is in front of it to clean, in order to physically clean what is in front of it, while the function keeps the entire history of percepts for its future decision making. <br> <br>

<b> What does PEAS stand for? </b> <br>
 Perfomance Measure, Environment, Actuator, Sensor
 <br> <br>

 <b> For each of the following activities, give a PEAS description of the task environment and specify the environment properties: playing soccer, performing high jumps, and bidding on an item at an auction (Undergraduate students take the first two and graduate students need to take all three).</b> <br>
Playing Soccer:<br>  Performance Measure: scoring, agility, speed, endurance, goal-defense <br>   Environment: soccer field, teammates/opponents, ball <br> Actuators: player bodies, kicking, blocking  <br>  Sensors: location, finding other players, ball locator, camera  <br>  Environment Properties: Partially observable, Deterministic, Sequential, Dynamic, Continous, Competitive Multiagent <br> <br>
Performing High Jumps:<br>  Performance Measure: Height, Height safety limit <br>   Environment: Horizontal Bar  <br>  Actuators: Body  <br>  Sensors: height measure, camera  <br>  Environment Properties: Fully observable, Non-Deterministic, Episodic, Semidynamic, Discrete, Single Agent <br>








